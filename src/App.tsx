import React from 'react';
import Tree from 'react-tree-graph';
import data from './data';
import newdata from './newdata';
import 'react-tree-graph/dist/style.css'
import './App.css';
import ReactDOM from 'react-dom';
import axios from 'axios';

const App: React.FC = (props: any) => {
  const handleClick = (event: any, node: any) => {
    console.log('handle click ', event);
    console.log('handle click node', node);
    debugger
    return ReactDOM.render(
    <Tree
      data={newdata}
      nodeRadius={15}
      margins={{ top: 20, bottom: 10, left: 20, right: 200 }}
      height={700}
      width={1000}/>, document.getElementById('root'));
  }

  const getRequest = () => {
    axios.get("localhost:8080/cookies")
    .then(response => {
      console.log(response);
      return response.data
    }).catch(error =>  console.log(error))
    }

  return (
    <Tree
      data={getRequest}
      nodeRadius={15}
      margins={{ top: 20, bottom: 10, left: 20, right: 200 }}
      gProps={{
	 className: 'node',
	 onClick: handleClick
      }}
      height={700}
      width={1000}/>
  );
}

export default App;
